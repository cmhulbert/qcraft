#!/usr/bin/env bash


VERSION=1.0.0
QCRAFT_PACKAGE_DIR=./QCraft_$VERSION
QCRAFT_DIR=.

# delete if already exists
rm -fr $QCRAFT_PACKAGE_DIR
# create new folder
mkdir $QCRAFT_PACKAGE_DIR

# Copy mods files into folder
cp -r $QCRAFT_DIR/locale $QCRAFT_PACKAGE_DIR/locale
cp -r $QCRAFT_DIR/prototypes $QCRAFT_PACKAGE_DIR/prototypes
cp $QCRAFT_DIR/control.lua $QCRAFT_PACKAGE_DIR/control.lua
cp $QCRAFT_DIR/data.lua $QCRAFT_PACKAGE_DIR/data.lua
cp $QCRAFT_DIR/info.json $QCRAFT_PACKAGE_DIR/info.json

zip -r $QCRAFT_PACKAGE_DIR.zip $QCRAFT_PACKAGE_DIR

rm -r $QCRAFT_PACKAGE_DIR



