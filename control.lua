---
--- Created by caleb.
--- DateTime: 7/19/19 5:47 PM
---

local function get_entity_ghost_name(event)
    player = game.players[event.player_index]
    if player ~= nil and player.selected ~= nil then
        return player.selected.ghost_name
    end
end

local function get_name_under_cursor(event)
    selected = event.selected_prototype
    if selected then
        if selected.base_type == "entity" then
            if selected.derived_type == "entity-ghost" then
                return get_entity_ghost_name(event)
            end
            return selected.name
        else
            if selected.base_type == "item" then
                return selected.name
            end
        end
    end
end

local function craft_one_from_cursor(event)
    target_name = get_name_under_cursor(event)
    player = game.players[event.player_index]
    if target_name ~= nil and player ~= nil then
        target_recipe = player.force.recipes[target_name]
        if target_recipe ~= nil then
            player.begin_crafting { recipe = target_recipe, count = 1 }
        end
    end
end

local function craft_five_from_cursor(event)
    target_name = get_name_under_cursor(event)
    player = game.players[event.player_index]
    if target_name ~= nil and player ~= nil then
        target_recipe = player.force.recipes[target_name]
        if target_recipe ~= nil then
            player.begin_crafting { recipe = target_recipe, count = 5 }
        end
    end
end

script.on_event("craft-one-from-cursor",
        function(event)
            pcall(craft_one_from_cursor, event)
        end
)

script.on_event("craft-five-from-cursor",
        function(event)
            pcall(craft_five_from_cursor, event)
        end
)

