---
--- Created by caleb.
--- DateTime: 7/19/19 6:16 PM
---
data:extend(
        {
            {
                type = "custom-input",
                name = "craft-one-from-cursor",
                key_sequence = "SHIFT + Q",
                action = "lua",
                include_selected_prototype = true
            },
            {
                type = "custom-input",
                name = "craft-five-from-cursor",
                key_sequence = "SHIFT + ALT + Q",
                action = "lua",
                include_selected_prototype = true
            }
        }
)